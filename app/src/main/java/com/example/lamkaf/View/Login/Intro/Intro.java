package com.example.lamkaf.View.Login.Intro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.lamkaf.R;
import com.example.lamkaf.databinding.ActivityIntroBinding;
import com.example.lamkaf.viewmodel.login_viewmodel.intro_viewmodel.IntroClickHandeler;
import com.example.lamkaf.viewmodel.login_viewmodel.intro_viewmodel.IntroViewModel;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class Intro extends AppCompatActivity {
       public static DotsIndicator dotsIndicator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_intro);
        ActivityIntroBinding activityIntroBinding= DataBindingUtil.setContentView(this,R.layout.activity_intro);
        IntroViewModel introViewModel=new IntroViewModel(this);
        activityIntroBinding.setIntroViewModel(introViewModel);
        dotsIndicator=findViewById(R.id.dots);
        IntroClickHandeler introClickHandeler=new IntroClickHandeler();
        activityIntroBinding.setHandel(introClickHandeler);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);


       //    dotsIndicator.setViewPager(introViewModel.getViewPager());



    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

    }
}
