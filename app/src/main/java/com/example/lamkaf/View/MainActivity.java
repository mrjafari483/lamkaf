package com.example.lamkaf.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.lamkaf.R;
import com.example.lamkaf.viewmodel.Spalsh_Screen_ViewModel;
import com.example.lamkaf.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        ActivityMainBinding activityMainBinding= DataBindingUtil.setContentView(this,R.layout.activity_main);
        Spalsh_Screen_ViewModel spalsh_screen_viewModel=new Spalsh_Screen_ViewModel(this);
        activityMainBinding.setSplashScreen(spalsh_screen_viewModel);


    }

}
