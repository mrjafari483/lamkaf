package com.example.lamkaf.View;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lamkaf.R;
import com.example.lamkaf.databinding.Horizontal_item;
import com.example.lamkaf.viewmodel.home.HomeViewModel;

import java.util.ArrayList;

public class Adapter_H_List extends RecyclerView.Adapter<Adapter_H_List.Adapter_View_holder> {
ArrayList<HomeViewModel> arrayList=new ArrayList<>();
LayoutInflater inflater;
    public Adapter_H_List(ArrayList<HomeViewModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public Adapter_View_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater==null){
            inflater=LayoutInflater.from(parent.getContext());
        }
Horizontal_item horizontal_item= DataBindingUtil.inflate(inflater, R.layout.horizontal_item,parent,false);

        return new Adapter_View_holder(horizontal_item);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_View_holder holder, int position) {
HomeViewModel homeViewModel=arrayList.get(position);
holder.bind(homeViewModel);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Adapter_View_holder extends  RecyclerView.ViewHolder{
        Horizontal_item horizontal_item;
        public Adapter_View_holder(Horizontal_item horizontal_item) {
            super(horizontal_item.getRoot());
this.horizontal_item=horizontal_item;
        }
        public void bind(HomeViewModel homeViewModel){
            this.horizontal_item.setHomeViewModel(homeViewModel);
            this.horizontal_item.executePendingBindings();
        }
    }
}
