package com.example.lamkaf.View.Login.Sinin_Sinup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.lamkaf.R;
import com.example.lamkaf.databinding.ActivitySinupBinding;
import com.example.lamkaf.viewmodel.login_viewmodel.sinin_sinup_viewmodel.Sinup_ViewModel;

public class Sinup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_sinup);
        ActivitySinupBinding activitySinupBinding= DataBindingUtil.setContentView(this,R.layout.activity_sinup);
        Sinup_ViewModel sinup_viewModel=new Sinup_ViewModel(this);
        activitySinupBinding.setSinupViewModel(sinup_viewModel);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);



    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

    }

}
