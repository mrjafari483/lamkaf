package com.example.lamkaf.View.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.lamkaf.R;
import com.example.lamkaf.databinding.ActivityHomeBinding;
import com.example.lamkaf.viewmodel.home.HomeViewModel;

public class HomeActivity extends AppCompatActivity {
ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_home);
        ActivityHomeBinding activityHomeBinding= DataBindingUtil.setContentView(this,R.layout.activity_home);
        HomeViewModel homeViewModel=new HomeViewModel(this);
        activityHomeBinding.setHomeViewModel(homeViewModel);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);

    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }

    }

