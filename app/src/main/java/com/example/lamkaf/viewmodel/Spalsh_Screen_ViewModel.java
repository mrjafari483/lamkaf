package com.example.lamkaf.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.databinding.BaseObservable;

import com.example.lamkaf.Model.SplashScreenModel;
import com.example.lamkaf.View.Login.Intro.Intro;
import com.example.lamkaf.View.home.HomeActivity;

public class Spalsh_Screen_ViewModel extends BaseObservable {
    Context context;
    String message;

    public Spalsh_Screen_ViewModel(SplashScreenModel splashScreenModel) {
        this.message = splashScreenModel.getMessage();
    }

    public Spalsh_Screen_ViewModel(Context context) {
        this.context = context;
        Intent i=new Intent(context, Intro.class);
        context.startActivity(i);

    }

}
