package com.example.lamkaf.viewmodel.login_viewmodel.intro_viewmodel;

import android.content.Context;
import android.view.View;
import android.widget.TableLayout;
import android.widget.Toast;

import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.lamkaf.R;
import com.example.lamkaf.View.Login.Intro.AapterIntro;
import com.example.lamkaf.View.Login.Intro.Intro;
import com.google.android.material.tabs.TabLayout;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class IntroViewModel extends BaseObservable {
    private static Context contextt;
    private Context context;
    private static ViewPager viewPagerr;



    private int[] layouts={R.layout.intro1,R.layout.intro2,R.layout.intro3};

    public IntroViewModel(Context context) {
        this.context = context;
        contextt=context;
    }

    @BindingAdapter("Bind:AdapterIntro")
    public static void bindAdapter(ViewPager viewPager , int[] layout ){
        AapterIntro adapter=new AapterIntro(layout,contextt);
        viewPager.setAdapter(adapter);
       Intro.dotsIndicator.setViewPager(viewPager);
       viewPagerr=viewPager;
    }

    public Context getContext() {
        return context;
    }

    public int[] getLayouts() {
        return layouts;
    }

}
