package com.example.lamkaf.viewmodel.login_viewmodel.intro_viewmodel;

import android.content.Intent;
import android.view.View;

import com.example.lamkaf.View.Login.Sinin_Sinup.Sinin;
import com.example.lamkaf.View.Login.Sinin_Sinup.Sinup;
import com.example.lamkaf.View.home.HomeActivity;

public class IntroClickHandeler {
    public void SinupClick(View view){
        Intent i=new Intent(view.getContext(), Sinup.class);
        view.getContext().startActivity(i);
    }
    public void SininClick(View view){
        Intent i=new Intent(view.getContext(), Sinin.class);
        view.getContext().startActivity(i);
    }
    public void SkipClick(View view){
        Intent i=new Intent(view.getContext(), HomeActivity.class);
        view.getContext().startActivity(i);
    }
}
