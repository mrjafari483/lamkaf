package com.example.lamkaf.viewmodel.home;

import android.content.Context;
import android.widget.Toast;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lamkaf.Model.Model_Rec_H_List;
import com.example.lamkaf.View.Adapter_H_List;

import java.util.ArrayList;

public class HomeViewModel extends ViewModel {
    int id;
    String imgUrl;
    String Category;
    String Title;
    String Source;
    Context context;

MutableLiveData<ArrayList<HomeViewModel>> mutableLiveData=new MutableLiveData<>();

    public MutableLiveData<ArrayList<HomeViewModel>> getMutableLiveData() {
        return mutableLiveData;
    }

    ArrayList<HomeViewModel> arrayList=new ArrayList<>();
    public HomeViewModel(Model_Rec_H_List model_rec_h_list) {
        this.id = model_rec_h_list.getId();
        this.imgUrl = model_rec_h_list.getImgUrl();
        this.Category = model_rec_h_list.getCategory();
        this.Title = model_rec_h_list.getTitle();
        this.Source = model_rec_h_list.getSource();
    }

    public HomeViewModel(Context context) {
        this.context = context;
        for (int i=0;i<50;i++){
            Model_Rec_H_List model_rec_h_list=new Model_Rec_H_List(i,"imgurl","کتاب","موضوع","سایت لام کام");
            HomeViewModel homeViewModel=new HomeViewModel(model_rec_h_list);
            arrayList.add(homeViewModel);
        }
        mutableLiveData.setValue(arrayList);
        Toast.makeText(context, "ok", Toast.LENGTH_SHORT).show();
    }
    @BindingAdapter("Bind:Adapter")
    public static void BindAdapter(RecyclerView recyclerView, MutableLiveData<ArrayList<HomeViewModel>> mutableLiveData){
        mutableLiveData.observe((LifecycleOwner) recyclerView.getContext(), new Observer<ArrayList<HomeViewModel>>() {
            @Override
            public void onChanged(ArrayList<HomeViewModel> homeViewModels) {
                Adapter_H_List adapter_h_list=new Adapter_H_List(homeViewModels);
recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(),LinearLayoutManager.HORIZONTAL,true));


recyclerView.setAdapter(adapter_h_list);
            }
        });
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }
}
