package com.example.lamkaf.Model;

public class Model_Rec_H_List {
    int id;
    String imgUrl;
    String Category;
    String Title;
   String Source;

    public Model_Rec_H_List(int id, String imgUrl, String category, String title, String source) {
        this.id = id;
        this.imgUrl = imgUrl;
        Category = category;
        Title = title;
        Source = source;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }
}
