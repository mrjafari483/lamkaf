package com.example.lamkaf.Model;

public class SplashScreenModel {
    String message;

    public SplashScreenModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
